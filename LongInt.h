#pragma once
class Longint {
public:
	static const int base = 10;
private:
	int* digits;
	int digits_len;
	//private methods
	int find_len(int value) {
		int index = 0;
		int max_possible = 0;
		while (max_possible < value) {
			max_possible += static_cast<int>(std::pow(Longint::base, index));
			index++;
		}
		return index;
	}

public:
	//constructors
	Longint();
	Longint(int value);


	//public methods
	void print();
};